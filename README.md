# Build.SBOL

Build.SBOL is an extension to [libSBOLj](https://github.com/SynBioDex/libSBOLj), enabling to specify operations regarding DNA synthesis and assembly A Build.SBOL specification is then being  mapped to the most up-to-date version of the [SBOL data model](http://sbolstandard.org/data-model-specifications/). Currently, 
Build.SBOL is mapped onto SBOL version [2.2.0](http://sbolstandard.org/wp-content/uploads/2016/10/BBF-RFC112-SBOL2.1.0.pdf).

## Build.SBOL operands

Build.SBOL supports the following operands:

* circular DNA constructs (e.g., plasmids/vectors)
 
* linear DNA constructs, which can be either 
    * double-stranded (e.g., synthetic DNA) or 
    * single-stranded (e.g., primers/oligos)
    
* enzymes with different functions, such as
    * restriction enzymes,
    * ligation enzymes,
    * amplification enzymes.

The operands must be represented as a SBOL ComponentDefinition.
 
## Build.SBOL operators

Build.SBOL supports the following operators:

- *cut*

    The cut operator serves to either cut a linear or circular DNA construct using a (restriction) enzyme.
    Biologically, the result of the cut operator are one or more linear constructs of DNA, depending 
    how often the (restriction) enzyme occurs in the DNA sequence of the input operands.
 
- *amplify*

    The amplify operator specifies the amplification process of either linear or circular constructs 
    using primers. In both cases, the result is a linear construct.

- *join*

    The join operator specifies the product of DNA assembly. Every operator is implemented as a static method of 
    the SBOLAssembly class. This result of this operator is an assembled product that can be further utilized 
    in the assembly process.
    
All SBOL.assembly operators are available via the SBOLAssembly class.
    
## Build.SBOL examples

The full source code of all examples can be found [here](https://bitbucket.org/berkeleylab/build.sbol/src/e5a0373dc35a/src/test/java/org/sbolstandard/build/demo/?at=master).
 
1. Insert a linear DNA construct into a circular vector.

```
// instantiate a SBOLDocument
SBOLDocument document = ... ;

// specify the elements of the build process, i.e.,
// vector, restriction enzyme, linear DNA, 5' and 3' primers

//---------------------------------------------------------------------
// BUILD.SBOL SPECIFICATIONS
//---------------------------------------------------------------------
// Operation 1: we linearize the vector using a restriction enzyme
linearized_vector = BuildSBOLOperators.cut(circular_vector, restriction_enzyme);

// Operation 2: we amplify the linear DNA construct with its 5' and 3' primers
amplified_construct = BuildSBOLOperators.amplify(linear_construct, five_primer, three_primer);

// Operation 3: we explicitly specify the join of the linearized_vector and the amplified_construct
assembled_product = BuildSBOLOperators.join(linearized_vector, amplified_construct);
//---------------------------------------------------------------------

```

The full source-code of this example can be found [here](https://bitbucket.org/berkeleylab/build.sbol/src/e5a0373dc35ae552c97f760b586573e710bfa825/src/test/java/org/sbolstandard/build/demo/InsertConstructIntoVector.java?at=master&fileviewer=file-view-default).

