grammar BuildSBOL;

options {
	language=Java;
}

@header {
// Package name
package org.sbolstandard.build.lang;

// Java Native Imports
import java.util.Set;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.LinkedHashMap;
import java.util.ArrayList;
import java.util.UUID;

import org.sbolstandard.build.dom.*;
}

@members {
private static final String NL = System.lineSeparator();
/**
 * 
 */
public void reportError(int line, final String msg) {
	StringBuilder sb = new StringBuilder();
	sb.append("Line ").append(line).append(": ").append(msg).append(NL);
	
	throw new IllegalArgumentException(sb.toString());
}
}

//--------------------------------------------------
// Parser Rules
//--------------------------------------------------
build_sbol_specification
	returns [Map<String, Operator> finalConstructs]
@init{
	$finalConstructs = new LinkedHashMap<String, Operator>();
}	
	:	( (displayId=ID EQUALS)? operator=build_instruction {

// generate the displayId of the final construct		
String finalConstructDisplayId = null;
if(null == $displayId) {
	finalConstructDisplayId = "uuid_" + UUID.randomUUID().toString().replaceAll("-", "_");
} else {
	finalConstructDisplayId = $displayId.text;
}

$finalConstructs.put(finalConstructDisplayId, $operator.operator);
	}	)+ 
	;

build_instruction
	returns [Operator operator]
	:	 op=( 'cut' | 'CUT' | 'amplify' | 'AMPLIFY' | 'join' | 'JOIN' | 'transform' | 'TRANSFORM' ) LEFTP ( loo=list_of_operands ) RIGHTP {

$operator = new Operator(
	BuildSBOLOperator.valueOf($op.text.toUpperCase()), $loo.operands);
	}	
	;

list_of_operands
	returns [List<LanguageElement> operands]
	@init {
$operands = new ArrayList<LanguageElement>();		
	}
	:	( id=ID {
		
Operand operand = new Operand($id.text);

$operands.add(operand);		
	}	| op=build_instruction {
$operands.add($op.operator);		
	}	) (COMMA loo=list_of_operands {
$operands.addAll($loo.operands);		
	}	)?
	;
	
//---------------------------------------------------
// Lexer Rules	
ID  :	('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|UNDERSCORE|MINUS|DOT)*
	;
	
LEFTP	
	:	'('
	;
	
RIGHTP
	:	')'
	;
	
COMMA
	: 	','
	;

UNDERSCORE
	:	'_'
	;

MINUS
	:	'-'
	;
	
DOT	:	'.'
	; 

EQUALS
	:	'='
	;
	

INT :	'0'..'9'+
    ;

FLOAT
    :   ('0'..'9')+ '.' ('0'..'9')* EXPONENT?
    |   DOT ('0'..'9')+ EXPONENT?
    |   ('0'..'9')+ EXPONENT
    ;

WS  :   ( ' '
        | '\t'
        ) -> skip
    ;

NEWLINE   
	:	( '\r'? '\n' ) -> skip 
	;

STRING
    :  '"' ( ESC_SEQ | ~('\\'|'"') )* '"'
    ;

CHAR:  '\'' ( ESC_SEQ | ~('\''|'\\') ) '\''
    ;

fragment
EXPONENT : ('e'|'E') ('+'|'-')? ('0'..'9')+ ;

fragment
HEX_DIGIT : ('0'..'9'|'a'..'f'|'A'..'F') ;

//     :   '\\' ('b'|'t'|'n'|'f'|'r'|'\"'|'\''|'\\')

fragment
ESC_SEQ
    :   '\\' ('b'||'n'|'f'||'\"'|'\''|'\\')
    |   UNICODE_ESC
    |   OCTAL_ESC
    ;

fragment
OCTAL_ESC
    :   '\\' ('0'..'3') ('0'..'7') ('0'..'7')
    |   '\\' ('0'..'7') ('0'..'7')
    |   '\\' ('0'..'7')
    ;

fragment
UNICODE_ESC
    :   '\\' 'u' HEX_DIGIT HEX_DIGIT HEX_DIGIT HEX_DIGIT
    ;