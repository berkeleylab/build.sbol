package org.sbolstandard.build;

import java.net.URI;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javax.xml.namespace.QName;

import org.sbolstandard.core2.ComponentDefinition;
import org.sbolstandard.core2.SequenceOntology;

/**
 * 
 * @author Ernst Oberortner
 */
public class BuildSBOLConstants {

	public static final String BUILD_SBOL_NAMESPACE = "http://sbolstandard.org/build/";
	public static final String BUILD_SBOL_PREFIX = "build";

	public static final QName OPERATOR_ANNOTATION = new QName(BUILD_SBOL_NAMESPACE, "operator", BUILD_SBOL_PREFIX);
//	public static final QName OPERATION_ANNOTATION = new QName(BUILD_SBOL_NAMESPACE, "operation", BUILD_SBOL_PREFIX);
	
	public static final URI CUT_OPERATION = URI.create(BUILD_SBOL_NAMESPACE + "cut");
	public static final URI AMPLIFY_OPERATION = URI.create(BUILD_SBOL_NAMESPACE + "amplify");
	public static final URI JOIN_OPERATION = URI.create(BUILD_SBOL_NAMESPACE + "join");
	
	public static final String VERSION = "1";
	
	public static final SequenceOntology so;
	static {
		 so = new SequenceOntology();
	}

	public static final URI DNA = ComponentDefinition.DNA;
	
	public static final Set<URI> LINEAR_SINGLE_STRANDED_DNA = 
			new HashSet<>(Arrays.asList(
					so.getURIbyId("SO:0000987"),		// linear
					so.getURIbyId("SO:0000984"),		// single-stranded
					ComponentDefinition.DNA));		// DNA
			
	public static final Set<URI> LINEAR_DOUBLE_STRANDED_DNA = 
			new HashSet<>(Arrays.asList(
					so.getURIbyId("SO:0000987"),		// linear
					so.getURIbyId("SO:0000985"),		// double-stranded
					ComponentDefinition.DNA));		// DNA

	public static final URI VECTOR_PLASMID = so.getURIbyId("SO:0000755");

	public static final URI PCR_PRODUCT = so.getURIbyId("SO:0000006");
	
	// the result of an amplification/PCR is a linear double-stranded DNA construct
	// we add also that an amplified construct is the product of a PCR reaction
	public static Set<URI> AMPLIFIED_CONSTRUCT = new HashSet<> (
			Arrays.asList(PCR_PRODUCT));
	static {
		AMPLIFIED_CONSTRUCT.addAll(LINEAR_DOUBLE_STRANDED_DNA);
	}

	public static final URI SYNTHETIC_OLIGO = so.getURIbyId("SO:0001247");
	public static final URI SYNTHETIC_SEQUENCE = so.getURIbyId("SO:0000351");
	public static final URI FORWARD_PRIMER = so.getURIbyId("SO:0000121");
	public static final URI REVERSE_PRIMER = so.getURIbyId("SO:0000132");
	
	// GeneOntology
	// "Type II site-specific deoxyribonuclease activity"
	public static final URI RESTRICTION_ENZYME = URI.create("http://purl.obolibrary.org/obo/GO_0009036");
	public static final URI POLYMERASE = URI.create("http://purl.obolibrary.org/obo/GO_0034061");

	public static final URI UPSTREAM = so.getURIbyId("SO:0001631");
	public static final URI DOWNSTREAM = so.getURIbyId("SO:0001632");

	public static final String BUILD_PREFIX = "http://sbolstandard.org/build/";
	
}
