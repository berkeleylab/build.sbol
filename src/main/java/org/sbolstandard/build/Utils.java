package org.sbolstandard.build;

import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.sbolstandard.core2.ComponentDefinition;
import org.sbolstandard.core2.SBOLDocument;
import org.sbolstandard.core2.SBOLValidationException;
import org.sbolstandard.core2.Sequence;

public class Utils {

	public static final String URI_PREFIX = "http://jgi.doe.gov/assembly/mockup-level-1";

	/**
	 * 
	 * @param displayId
	 * @param types
	 * @param sequence
	 * @param sequenceType
	 * @return
	 * @throws SBOLValidationException 
	 */
	public static ComponentDefinition createComponentDefinition(
			final SBOLDocument document,
			final String displayId, final Set<URI> types, 
			final String sequence, final URI sequenceType) 
					throws SBOLValidationException {

		String uriPrefix = document.getDefaultURIprefix();

		ComponentDefinition cd = document.createComponentDefinition(
				uriPrefix, displayId, "1", types);

		Sequence cdSequence = document.createSequence(
				uriPrefix,						// URI's prefix
				displayId + "_sequence",		// displayID
				"1",							// version
				sequence, 						// elements
				sequenceType);					// encoding
		cd.addSequence(cdSequence.getIdentity());

		return cd;
	}
	
	/**
	 * @return a random UUID that has all '-' replaced with '_' in order to 
	 * have a SBOL compliant displayId
	 */
	public static String getRandomDisplayId() {
		return UUID.randomUUID().toString().replaceAll("-", "_");
	}
	
	
	//----------------------------------------------------------
	// SEQUENCE-related METHODS
	//
	
	/**
	 * 
	 * @param construct
	 * @return
	 */
	public static Sequence getSequence(final SBOLDocument document, final ComponentDefinition construct) {

		// get the construct's sequence
		Set<Sequence> cdSequences = construct.getSequences();
		Sequence sequence = (Sequence)null;
		// either the construct has at least one sequence
		if(!cdSequences.isEmpty()) {
			// if so, then we just take the first one
			sequence = new ArrayList<Sequence>(cdSequences).get(0);
		} else {
			// if the construct does not have a sequence, 
			// then we build it (based on it's sub-components)
			cdSequences = buildSequence(document, construct);

			sequence = new ArrayList<Sequence>(cdSequences).get(0);
		}
		
		return sequence;
	}


	/**
	 * 
	 * @param construct
	 * @return
	 */
	public static Set<Sequence> buildSequence(final SBOLDocument document, final ComponentDefinition construct) {
	
		// currently, that's a "little" hack
		
		// ideally, we have to traverse the tree of the construct's sub-components
		ComponentDefinition cd = document.getComponentDefinition(
				construct.getDisplayId() + "_A_01", "1");

		if(null != cd) {
			return cd.getSequences();
		}
		
		return (Set<Sequence>)null;
	}
	//----------------------------------------------------------
	
	
	
	/**
	 * designing primers of a DNA sequence construct depending on the construct's orientation
	 * 
	 * @param document ... the SBOL document
	 * @param construct ... the DNA sequence construct
	 * @param orientation ... the orientation of the construct
	 * 
	 * @return a list containing the 5' and 3' primers (represented as ComponentDefinitions)
	 * 
	 * @throws SBOLValidationException
	 */
	public static List<ComponentDefinition> primerDesign(
			final SBOLDocument document, final ComponentDefinition construct, final Orientations orientation)
				throws SBOLValidationException {

		List<ComponentDefinition> primers = new ArrayList<>();
		switch(orientation) {
		case FORWARD:
			primers = forwardPrimerDesign(document, construct);
			break;
		case REVERSE:
			primers = reversePrimerDesign(document, construct);
			break;
		default:
			throw new IllegalArgumentException(orientation + " is an invalid orientation!");
		}
		
		if(primers.isEmpty()) {
			throw new IllegalArgumentException("Cannot determine sequence for " + construct.getDisplayId());
		}
		
		return primers;
	}

	
	public static List<ComponentDefinition> forwardPrimerDesign(
			final SBOLDocument document, final ComponentDefinition construct)
				throws SBOLValidationException {

		List<ComponentDefinition> primers = new ArrayList<>();		
		
		Sequence sequence = getSequence(document, construct);
		if(null != sequence) {
			// 5' primer is the forward primer
			String fivePrimePrimer = sequence.getElements().substring(0,2).toLowerCase();
			primers.add(
					createComponentDefinition(document, 
							construct.getDisplayId() + "_F", 
							new HashSet<URI>(Arrays.asList(new URI[] {
									ComponentDefinition.DNA, Types.LINEAR.getURI(), Types.SINGLE_STRANDED.getURI()})),
							fivePrimePrimer, Sequence.IUPAC_DNA));
			
			// 3' primer is the reverse primer
			String s = sequence.getElements();
			char c1 = s.charAt(s.length() - 1);
			char c2 = s.charAt(s.length() - 2);
			String pr = String.valueOf(c1) + String.valueOf(c2);
			String threePrimePrimer = Sequence.reverseComplement(pr.toLowerCase(), ComponentDefinition.DNA);
			primers.add(
					createComponentDefinition(document, 
							construct.getDisplayId() + "_R", 
							new HashSet<URI>(Arrays.asList(new URI[] {
									ComponentDefinition.DNA, Types.LINEAR.getURI(), Types.SINGLE_STRANDED.getURI()})),
							threePrimePrimer, Sequence.IUPAC_DNA));
		}
		
		return primers;
	}
	
	public static List<ComponentDefinition> reversePrimerDesign(
			final SBOLDocument document, final ComponentDefinition construct)
				throws SBOLValidationException {
		
		List<ComponentDefinition> primers = new ArrayList<>();
		
		Sequence sequence = getSequence(document, construct);
		if(null != sequence) {
			// 5' primer is the reverse primer
			String s = sequence.getElements().substring(0,2);
			String revComp = Sequence.reverseComplement(s.toLowerCase(), ComponentDefinition.DNA);
			String fivePrimePrimer = revComp;
			primers.add(
					createComponentDefinition(document, 
							construct.getDisplayId() + "_R", 
							new HashSet<URI>(Arrays.asList(new URI[] {
									ComponentDefinition.DNA, Types.LINEAR.getURI(), Types.SINGLE_STRANDED.getURI()})),
							fivePrimePrimer, Sequence.IUPAC_DNA));
			
			// 3' primer is the forward primer
			s = sequence.getElements();
			char c1 = s.charAt(s.length() - 1);
			char c2 = s.charAt(s.length() - 2);
			String threePrimePrimer = (String.valueOf(c1) + String.valueOf(c2)).toLowerCase();

			primers.add(
					createComponentDefinition(document, 
							construct.getDisplayId() + "_F", 
							new HashSet<URI>(Arrays.asList(new URI[] {
									ComponentDefinition.DNA, Types.LINEAR.getURI(), Types.SINGLE_STRANDED.getURI()})),
							threePrimePrimer, Sequence.IUPAC_DNA));
		}
		
		return primers;
	}
}
