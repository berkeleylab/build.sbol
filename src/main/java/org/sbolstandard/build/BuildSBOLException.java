package org.sbolstandard.build;

/**
 * The BuildSBOLException class represents exceptions 
 * that occur while parsing and interpreting an Build.SBOL s
 * specification. It outputs the error message in the following format:
 * Line <line-number>: <error-message>
 * 
 * @author Ernst Oberortner
 */
public class BuildSBOLException 
		extends Exception {

	private static final long serialVersionUID = 7362206942890933783L;
	private int line;
	private int pos;
	
	/**
	 * 
	 * @param message
	 */
	public BuildSBOLException(final String message) {
		this(1, 1, message);
	}
	
	/**
	 * 
	 * @param line
	 * @param pos
	 * @param message
	 */
	public BuildSBOLException(int line, int pos, final String message) {
		super(message);
		this.line = line;
		this.pos = pos;
	}
	
	public int getLine() {
		return this.line;
	}
	
	public int getPosition() {
		return this.pos;
	}
	
	@Override
	public String getLocalizedMessage() {
		return String.format("Line %d:%d: %s", this.getLine(), this.getPosition(), this.getMessage());
	}
}
