package org.sbolstandard.build;

import org.antlr.v4.runtime.BaseErrorListener;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;

import java.util.ArrayList;
import java.util.List;

public class BuildSBOLSyntaxErrorListener 
		extends BaseErrorListener {

	private List<BuildSBOLException> syntaxErrors;
	
    @Override
    public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol,
                            int line, int pos, String msg, RecognitionException e) {

        if(null == this.syntaxErrors) {
        		this.syntaxErrors = new ArrayList<BuildSBOLException>();
        }

        this.syntaxErrors.add(
        		new BuildSBOLException(line, pos, msg));
    }
    
    
    public List<BuildSBOLException> getSyntaxErrors() {
    		return this.syntaxErrors;
    }
}
