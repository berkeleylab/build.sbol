package org.sbolstandard.build;

import java.util.List;
import java.util.Map;

import org.sbolstandard.build.dom.Operator;

/**
 * The BuildSBOLParserResults wraps the results received 
 * from the ANTLR-generated parser. It either contains 
 * a list of build.sbol operators or a list of exceptions.
 * 
 * @author Ernst Oberortner
 */
public class ParseResults {
	
	// a map, where each key represents a specified build instruction and 
	// the corresponding value is the Build.SBOL operator
	public Map<String, Operator> buildSpecifications;
	
	// a list of exceptions occurred during the parsing process
	public List<BuildSBOLException> exceptions;
	
	/**
	 * Constructor
	 */
	public ParseResults() {
		this.buildSpecifications = null;
		this.exceptions = null;
	}
}
