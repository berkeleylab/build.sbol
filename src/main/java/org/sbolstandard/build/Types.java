package org.sbolstandard.build;

import java.net.URI;

public enum Types {
	
	LINEAR {
		@Override
		public URI getURI() {
			return URI.create("http://purl.obolibrary.org/obo/SO_0000987");
		}
	},
	
	CIRCULAR {
		@Override
		public URI getURI() {
			return URI.create("http://purl.obolibrary.org/obo/SO_0000988");
		}
	},
	
	SINGLE_STRANDED {
		@Override
		public URI getURI() {
			return URI.create("http://purl.obolibrary.org/obo/SO_0000984");
		}
	},
	
	DOUBLE_STRANDED {
		@Override
		public URI getURI() {
			return URI.create("http://purl.obolibrary.org/obo/SO_0000985");
		}
	};
	
	public abstract URI getURI();

}
