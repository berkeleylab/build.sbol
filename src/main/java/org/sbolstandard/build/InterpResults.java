package org.sbolstandard.build;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.sbolstandard.build.dom.Operator;
import org.sbolstandard.core2.ComponentDefinition;

/**
 * for keeping track of the results of interpreting 
 * parsed BuildSBOL specifications. The results include:
 * -- the operators of the BuildSBOL specification and their corresponding 
 *    ComponentDefinition
 * -- exceptions
 *  
 * @author Ernst Oberortner
 */
public class InterpResults {
	
	// a list of ComponentDefinition objects each 
	// representing one "top-level" operator
	// Example: join(amplify(),cut()) 
	// Here, the list will contain one ComponentDefinition, namely
	// the join operator's ComponentDefinition
	private Map<Operator, Set<ComponentDefinition>> operatorsAndComponentDefinitions;
	
	// a list of exceptions occurred during the parsing process
	private List<BuildSBOLException> exceptions;
	
	/**
	 * Constructor
	 */
	public InterpResults() {
		this.operatorsAndComponentDefinitions = null;
		this.exceptions = null;
	}
	
	/**
	 * @return ... a hash-map representing the operators and their corresponding 
	 * ComponentDefinition objects
	 */
	public Map<Operator, Set<ComponentDefinition>> getOperatorsAndComponentDefinitions() {
		return this.operatorsAndComponentDefinitions;
	}
	
	/**
	 * puts an operator and its corresponding ComponentDefinition into the hash-map. 
	 * If the operator exists in the hash-table already, then the given ComponentDefinition 
	 * will not be inserted into the hash-table.  
	 * 
	 * @param operator ... the operator
	 * @param componentDefinition ... the operator's ComponentDefinition
	 * 
	 */
	public void addOperatorComponentDefinition(final Operator operator, final Set<ComponentDefinition> componentDefinition) {

		// lazy evaluation of the hash-map
		if(null == this.getOperatorsAndComponentDefinitions()) {
			this.operatorsAndComponentDefinitions = new HashMap<Operator, Set<ComponentDefinition>>();
		}
		
		// if the operator exists already, then we do not add it again
		if(!this.operatorsAndComponentDefinitions.containsKey(operator)) {
			this.operatorsAndComponentDefinitions.put(operator, componentDefinition);
		}
	}
	
	/**
	 * @return ... a list of exceptions occurred during the interpreting process
	 */
	public List<BuildSBOLException> getExceptions() {
		return this.exceptions;
	}
	
	/**
	 * adds an exception to the list of exceptions
	 * 
	 * @param exception ... the exception to be added
	 */
	public void addException(final BuildSBOLException exception) {
		if(null == this.getExceptions()) {
			this.exceptions = new ArrayList<BuildSBOLException>();
		}
		
		this.exceptions.add(exception);
	}
	
}
