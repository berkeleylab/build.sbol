// Generated from BuildSBOL.g4 by ANTLR 4.4

// Package name
package org.sbolstandard.build.lang;

// Java Native Imports
import java.util.Set;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.LinkedHashMap;
import java.util.ArrayList;
import java.util.UUID;

import org.sbolstandard.build.dom.*;

import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link BuildSBOLParser}.
 */
public interface BuildSBOLListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link BuildSBOLParser#list_of_operands}.
	 * @param ctx the parse tree
	 */
	void enterList_of_operands(@NotNull BuildSBOLParser.List_of_operandsContext ctx);
	/**
	 * Exit a parse tree produced by {@link BuildSBOLParser#list_of_operands}.
	 * @param ctx the parse tree
	 */
	void exitList_of_operands(@NotNull BuildSBOLParser.List_of_operandsContext ctx);
	/**
	 * Enter a parse tree produced by {@link BuildSBOLParser#build_instruction}.
	 * @param ctx the parse tree
	 */
	void enterBuild_instruction(@NotNull BuildSBOLParser.Build_instructionContext ctx);
	/**
	 * Exit a parse tree produced by {@link BuildSBOLParser#build_instruction}.
	 * @param ctx the parse tree
	 */
	void exitBuild_instruction(@NotNull BuildSBOLParser.Build_instructionContext ctx);
	/**
	 * Enter a parse tree produced by {@link BuildSBOLParser#build_sbol_specification}.
	 * @param ctx the parse tree
	 */
	void enterBuild_sbol_specification(@NotNull BuildSBOLParser.Build_sbol_specificationContext ctx);
	/**
	 * Exit a parse tree produced by {@link BuildSBOLParser#build_sbol_specification}.
	 * @param ctx the parse tree
	 */
	void exitBuild_sbol_specification(@NotNull BuildSBOLParser.Build_sbol_specificationContext ctx);
}