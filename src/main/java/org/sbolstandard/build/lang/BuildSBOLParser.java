// Generated from BuildSBOL.g4 by ANTLR 4.4

// Package name
package org.sbolstandard.build.lang;

// Java Native Imports
import java.util.Set;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.LinkedHashMap;
import java.util.ArrayList;
import java.util.UUID;

import org.sbolstandard.build.dom.*;

import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class BuildSBOLParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.4", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__7=1, T__6=2, T__5=3, T__4=4, T__3=5, T__2=6, T__1=7, T__0=8, ID=9, 
		LEFTP=10, RIGHTP=11, COMMA=12, UNDERSCORE=13, MINUS=14, DOT=15, EQUALS=16, 
		INT=17, FLOAT=18, WS=19, NEWLINE=20, STRING=21, CHAR=22;
	public static final String[] tokenNames = {
		"<INVALID>", "'CUT'", "'cut'", "'transform'", "'AMPLIFY'", "'TRANSFORM'", 
		"'JOIN'", "'amplify'", "'join'", "ID", "'('", "')'", "','", "'_'", "'-'", 
		"'.'", "'='", "INT", "FLOAT", "WS", "NEWLINE", "STRING", "CHAR"
	};
	public static final int
		RULE_build_sbol_specification = 0, RULE_build_instruction = 1, RULE_list_of_operands = 2;
	public static final String[] ruleNames = {
		"build_sbol_specification", "build_instruction", "list_of_operands"
	};

	@Override
	public String getGrammarFileName() { return "BuildSBOL.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }


	private static final String NL = System.lineSeparator();
	/**
	 * 
	 */
	public void reportError(int line, final String msg) {
		StringBuilder sb = new StringBuilder();
		sb.append("Line ").append(line).append(": ").append(msg).append(NL);
		
		throw new IllegalArgumentException(sb.toString());
	}

	public BuildSBOLParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class Build_sbol_specificationContext extends ParserRuleContext {
		public Map<String, Operator> finalConstructs;
		public Token displayId;
		public Build_instructionContext operator;
		public Build_instructionContext build_instruction(int i) {
			return getRuleContext(Build_instructionContext.class,i);
		}
		public List<TerminalNode> EQUALS() { return getTokens(BuildSBOLParser.EQUALS); }
		public List<TerminalNode> ID() { return getTokens(BuildSBOLParser.ID); }
		public List<Build_instructionContext> build_instruction() {
			return getRuleContexts(Build_instructionContext.class);
		}
		public TerminalNode EQUALS(int i) {
			return getToken(BuildSBOLParser.EQUALS, i);
		}
		public TerminalNode ID(int i) {
			return getToken(BuildSBOLParser.ID, i);
		}
		public Build_sbol_specificationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_build_sbol_specification; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BuildSBOLListener ) ((BuildSBOLListener)listener).enterBuild_sbol_specification(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BuildSBOLListener ) ((BuildSBOLListener)listener).exitBuild_sbol_specification(this);
		}
	}

	public final Build_sbol_specificationContext build_sbol_specification() throws RecognitionException {
		Build_sbol_specificationContext _localctx = new Build_sbol_specificationContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_build_sbol_specification);

			((Build_sbol_specificationContext)_localctx).finalConstructs =  new LinkedHashMap<String, Operator>();

		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(13); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(8);
				_la = _input.LA(1);
				if (_la==ID) {
					{
					setState(6); ((Build_sbol_specificationContext)_localctx).displayId = match(ID);
					setState(7); match(EQUALS);
					}
				}

				setState(10); ((Build_sbol_specificationContext)_localctx).operator = build_instruction();


				// generate the displayId of the final construct		
				String finalConstructDisplayId = null;
				if(null == ((Build_sbol_specificationContext)_localctx).displayId) {
					finalConstructDisplayId = "uuid_" + UUID.randomUUID().toString().replaceAll("-", "_");
				} else {
					finalConstructDisplayId = (((Build_sbol_specificationContext)_localctx).displayId!=null?((Build_sbol_specificationContext)_localctx).displayId.getText():null);
				}

				_localctx.finalConstructs.put(finalConstructDisplayId, ((Build_sbol_specificationContext)_localctx).operator.operator);
					
				}
				}
				setState(15); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__7) | (1L << T__6) | (1L << T__5) | (1L << T__4) | (1L << T__3) | (1L << T__2) | (1L << T__1) | (1L << T__0) | (1L << ID))) != 0) );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Build_instructionContext extends ParserRuleContext {
		public Operator operator;
		public Token op;
		public List_of_operandsContext loo;
		public TerminalNode RIGHTP() { return getToken(BuildSBOLParser.RIGHTP, 0); }
		public List_of_operandsContext list_of_operands() {
			return getRuleContext(List_of_operandsContext.class,0);
		}
		public TerminalNode LEFTP() { return getToken(BuildSBOLParser.LEFTP, 0); }
		public Build_instructionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_build_instruction; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BuildSBOLListener ) ((BuildSBOLListener)listener).enterBuild_instruction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BuildSBOLListener ) ((BuildSBOLListener)listener).exitBuild_instruction(this);
		}
	}

	public final Build_instructionContext build_instruction() throws RecognitionException {
		Build_instructionContext _localctx = new Build_instructionContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_build_instruction);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(17);
			((Build_instructionContext)_localctx).op = _input.LT(1);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__7) | (1L << T__6) | (1L << T__5) | (1L << T__4) | (1L << T__3) | (1L << T__2) | (1L << T__1) | (1L << T__0))) != 0)) ) {
				((Build_instructionContext)_localctx).op = (Token)_errHandler.recoverInline(this);
			}
			consume();
			setState(18); match(LEFTP);
			{
			setState(19); ((Build_instructionContext)_localctx).loo = list_of_operands();
			}
			setState(20); match(RIGHTP);


			((Build_instructionContext)_localctx).operator =  new Operator(
				BuildSBOLOperator.valueOf((((Build_instructionContext)_localctx).op!=null?((Build_instructionContext)_localctx).op.getText():null).toUpperCase()), ((Build_instructionContext)_localctx).loo.operands);
				
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class List_of_operandsContext extends ParserRuleContext {
		public List<LanguageElement> operands;
		public Token id;
		public Build_instructionContext op;
		public List_of_operandsContext loo;
		public TerminalNode ID() { return getToken(BuildSBOLParser.ID, 0); }
		public List_of_operandsContext list_of_operands() {
			return getRuleContext(List_of_operandsContext.class,0);
		}
		public Build_instructionContext build_instruction() {
			return getRuleContext(Build_instructionContext.class,0);
		}
		public TerminalNode COMMA() { return getToken(BuildSBOLParser.COMMA, 0); }
		public List_of_operandsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_list_of_operands; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BuildSBOLListener ) ((BuildSBOLListener)listener).enterList_of_operands(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BuildSBOLListener ) ((BuildSBOLListener)listener).exitList_of_operands(this);
		}
	}

	public final List_of_operandsContext list_of_operands() throws RecognitionException {
		List_of_operandsContext _localctx = new List_of_operandsContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_list_of_operands);

		((List_of_operandsContext)_localctx).operands =  new ArrayList<LanguageElement>();		
			
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(28);
			switch (_input.LA(1)) {
			case ID:
				{
				setState(23); ((List_of_operandsContext)_localctx).id = match(ID);

						
				Operand operand = new Operand((((List_of_operandsContext)_localctx).id!=null?((List_of_operandsContext)_localctx).id.getText():null));

				_localctx.operands.add(operand);		
					
				}
				break;
			case T__7:
			case T__6:
			case T__5:
			case T__4:
			case T__3:
			case T__2:
			case T__1:
			case T__0:
				{
				setState(25); ((List_of_operandsContext)_localctx).op = build_instruction();

				_localctx.operands.add(((List_of_operandsContext)_localctx).op.operator);		
					
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			setState(34);
			_la = _input.LA(1);
			if (_la==COMMA) {
				{
				setState(30); match(COMMA);
				setState(31); ((List_of_operandsContext)_localctx).loo = list_of_operands();

				_localctx.operands.addAll(((List_of_operandsContext)_localctx).loo.operands);		
					
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3\30\'\4\2\t\2\4\3"+
		"\t\3\4\4\t\4\3\2\3\2\5\2\13\n\2\3\2\3\2\3\2\6\2\20\n\2\r\2\16\2\21\3\3"+
		"\3\3\3\3\3\3\3\3\3\3\3\4\3\4\3\4\3\4\3\4\5\4\37\n\4\3\4\3\4\3\4\3\4\5"+
		"\4%\n\4\3\4\2\2\5\2\4\6\2\3\3\2\3\n\'\2\17\3\2\2\2\4\23\3\2\2\2\6\36\3"+
		"\2\2\2\b\t\7\13\2\2\t\13\7\22\2\2\n\b\3\2\2\2\n\13\3\2\2\2\13\f\3\2\2"+
		"\2\f\r\5\4\3\2\r\16\b\2\1\2\16\20\3\2\2\2\17\n\3\2\2\2\20\21\3\2\2\2\21"+
		"\17\3\2\2\2\21\22\3\2\2\2\22\3\3\2\2\2\23\24\t\2\2\2\24\25\7\f\2\2\25"+
		"\26\5\6\4\2\26\27\7\r\2\2\27\30\b\3\1\2\30\5\3\2\2\2\31\32\7\13\2\2\32"+
		"\37\b\4\1\2\33\34\5\4\3\2\34\35\b\4\1\2\35\37\3\2\2\2\36\31\3\2\2\2\36"+
		"\33\3\2\2\2\37$\3\2\2\2 !\7\16\2\2!\"\5\6\4\2\"#\b\4\1\2#%\3\2\2\2$ \3"+
		"\2\2\2$%\3\2\2\2%\7\3\2\2\2\6\n\21\36$";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}