package org.sbolstandard.build;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.sbolstandard.build.dom.LanguageElement;
import org.sbolstandard.build.dom.Operand;
import org.sbolstandard.build.dom.Operator;
import org.sbolstandard.build.lang.BuildSBOLLexer;
import org.sbolstandard.build.lang.BuildSBOLParser;
import org.sbolstandard.build.lang.BuildSBOLParser.Build_sbol_specificationContext;
import org.sbolstandard.core2.ComponentDefinition;
import org.sbolstandard.core2.SBOLDocument;
import org.sbolstandard.core2.SBOLValidationException;

/**
 * The BuildSBOLExchanger serves the purpose of
 * parsing an interpreting a build.sbol file
 * 
 * @author Ernst Oberortner
 */
public class BuildSBOLAdapter {

	/**
	 * reads the build instructions from the file with the given filename and 
	 * stores the resulting data in the provided SBOLDocument.
	 * 
	 * @param document ... the SBOLDocument that should be populated with 
	 * the build instructions
	 * @param filename ... the filename of the file that contains the build instructions
	 * 
	 * @throws IOException
	 */
	public static ParseResults parse(final String filename) 
			throws IOException {
		return parse(Paths.get(filename));
	}
	
	/**
	 * parses the build instructions from the given file and
	 * returns.
	 * 
	 * @param document ... the SBOLDocument that should be populated with 
	 * the build instructions
	 * @param file ... the file that contains the build instructions
	 * 
	 * @throws IOException
	 */
	public static ParseResults parse(final Path file) 
			throws IOException {
		return parse(Files.newInputStream(file));
	}
	
	/**
	 * parses the build instructions from a given stream 
	 * 
	 * @param is ... the input stream to be parsed
	 * 
	 * @return ... a list of operators describing the 
	 * build instructions
	 */
	public static ParseResults parse(final InputStream is) {

		ParseResults parserResult = null;
		
		try (
				BufferedReader br = new BufferedReader(new InputStreamReader(is));
			) {
			
			// lexxer
			BuildSBOLLexer lexer = new BuildSBOLLexer(new ANTLRInputStream(br));
			CommonTokenStream tokens = new CommonTokenStream(lexer);
			
			// parser
			BuildSBOLParser parser = new BuildSBOLParser(tokens);
			
			BuildSBOLSyntaxErrorListener listener = new BuildSBOLSyntaxErrorListener();

			parser.removeErrorListeners();
			parser.addErrorListener(listener);

			// do the parsing
			Build_sbol_specificationContext parserReturn = parser.build_sbol_specification();

			// if there were syntax errors,
			parserResult = new ParseResults();
			if(null != listener.getSyntaxErrors()) {
				parserResult.exceptions = listener.getSyntaxErrors();
			} else {
				parserResult.buildSpecifications = parserReturn.finalConstructs;
			}
			
		} catch(Exception e) {
			
			// seems like something went wrong while reading the input stream
			parserResult.exceptions = new ArrayList<BuildSBOLException>();
			parserResult.exceptions.add(new BuildSBOLException(1, 1, e.getMessage()));
			
		}
		
		return parserResult;

	}
	
	/**
	 * Interprets a given list of operators. That is, for every operator, 
	 * it performs specifying the corresponding operation in the given SBOLDocument. 
	 * Also, it queries the given document for the operands of any operator.
	 * 
	 * @param document   ... the document that contains the operands and will contain the 
	 * specification of the operations
	 * @param operators  ... the operators to be interpreted
	 * 
	 * @throws SBOLValidationException 
	 */
	public static InterpResults interpret(
			final SBOLDocument document, final Map<String, Operator> finalConstructs) 
					throws SBOLValidationException {
		
		InterpResults results = new InterpResults();

		// Build.SBOL namespace
		document.addNamespace(URI.create(BuildSBOLConstants.BUILD_SBOL_NAMESPACE), BuildSBOLConstants.BUILD_SBOL_PREFIX);

		// for every given operator
		for(String displayId : finalConstructs.keySet()) {
		
			Operator operator = finalConstructs.get(displayId);

			try {
				
				// interpret the operator
				Set<ComponentDefinition> cd = interpretOperator(document, displayId, operator);

				// and store the resulting ComponentDefinition in the results
				results.addOperatorComponentDefinition(operator, cd);
				
			} catch (BuildSBOLException e) {
				results.addException(e);
			}
		}
		
		return results;
	}

	/**
	 * interprets an operator by instantiating a ComponentDefinition for each of its operands
	 * 
	 * @param document ... the SBOLDocument
	 * @param displayId ... the displayId that the resulting ComponentDefinition should have
	 * @param operator ... the operator to be interpreted
	 * 
	 * @return ... the resulting ComponentDefinitions
	 * 
	 * @throws BuildSBOLException
	 */
	public static Set<ComponentDefinition> interpretOperator(final SBOLDocument document, final String displayId, final Operator operator) 
			throws BuildSBOLException {

		// interpret its operands
		List<ComponentDefinition> cdOperands = new ArrayList<>();
		for(LanguageElement element : operator.getOperands()) {
			Set<ComponentDefinition> cds = interpret(document, element);
			if(null != cds) {
				cdOperands.addAll(cds);
			}
		}

		// generate the displayId of the joined construct
		String cdDisplayId = displayId;
		if(null == cdDisplayId) {
			String operatorDescription = operator.getOperator().toString().toUpperCase();
			StringBuilder sb = new StringBuilder();
			sb.append(operatorDescription).append("__");
			int i = 0;
			for(ComponentDefinition construct : cdOperands) {
				sb.append(construct.getDisplayId());
				if(i < cdOperands.size() - 1) {
					sb.append("__");
				}
				i++;
			}
			cdDisplayId = sb.toString();
		}

//		// check if a CD with the displayId exists already
//		ComponentDefinition existingCD = document.getComponentDefinition(cdDisplayId, "1");
//		if(null != existingCD) {
//			return existingCD;
//		}

		// depending on the operand, call the proper BuildSBOL method
		Set<ComponentDefinition> cd = null;
		try {
			switch(operator.getOperator()) {
			case JOIN:
				cd = new HashSet<ComponentDefinition>();
				cd.add(BuildSBOLOperators.join(document, cdDisplayId, cdOperands));
				break;
				
			case AMPLIFY:
				if(cdOperands.size() < 2 || cdOperands.size() > 3) {
					throw new BuildSBOLException("Invalid number of operands! The amplify operator takes either two or three operands.");
				}
				
				cd = new HashSet<ComponentDefinition>();
				if(cdOperands.size() == 3) {
					cd.add(BuildSBOLOperators.amplify(document, cdDisplayId, cdOperands.get(0), cdOperands.get(1), cdOperands.get(2)));
				} else {
					cd.add(BuildSBOLOperators.amplify(document, cdDisplayId, cdOperands.get(0), cdOperands.get(1)));
				}
				break;
						
			case CUT:
				if(cdOperands.size() != 2) {
					throw new BuildSBOLException("Invalid number of operands! The cut operator takes two operands.");
				}
				cd = BuildSBOLOperators.cut(document, cdDisplayId, cdOperands.get(0), cdOperands.get(1));
				break;
				
			case TRANSFORM:
				throw new IllegalArgumentException("The transform operator is not implemented yet!");
			}
			
//			// "log" the specified operation as annotation in the SBOL document
//			cd.createAnnotation(BuildSBOLConstants.OPERATION_ANNOTATION, operator.toString());
			
		} catch(SBOLValidationException e) {
			throw new BuildSBOLException(e.getMessage());
		}
		
		return cd;
	}
	
	
	/**
	 * 
	 * @param document
	 * @param element
	 * 
	 * @throws BuildSBOLException 
	 */
	public static Set<ComponentDefinition> interpret(final SBOLDocument document, final LanguageElement element) 
			throws BuildSBOLException {
		if(element instanceof Operator) {
			return interpretOperator(document, null, (Operator)element);
		} else if(element instanceof Operand) {
			Set<ComponentDefinition> cds = new HashSet<>();
			cds.add(interpretOperand(document, (Operand)element));
			return cds;
		}
		return (Set<ComponentDefinition>)null;
	}
	
	/**
	 * retrieves the given operand from the given document
	 * 
	 * @param document ... the SBOLDocument
	 * @param operand  ... the operand
	 */
	public static ComponentDefinition interpretOperand(final SBOLDocument document, final Operand operand) 
			throws BuildSBOLException {

		String displayId = operand.getDisplayId().replaceAll("\\.", "_");

		// get the ComponentDefinition from the given document
		// using the operand's displayID
		ComponentDefinition cdOperand = document.getComponentDefinition(displayId, "1");
		if(null == cdOperand) {
			throw new BuildSBOLException("Invalid operand! "
					+ "The operand " + displayId + " does not exist in the SBOL document.");
		}
		return cdOperand; 
	}
}
