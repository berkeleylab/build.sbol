package org.sbolstandard.build.dom;

/**
 * The base-class of the Build.SBOL language
 * 
 * @author Ernst Oberortner
 */
public abstract class LanguageElement {

	private String displayId;

	/**
	 * Constructor
	 *  
	 * @param displayId ... the displayId of the language element
	 */
	public LanguageElement(final String displayId) {
		this.displayId = displayId;
	}
	
	/**
	 * @return
	 */
	public String getDisplayId() {
		return this.displayId;
	}
	
	@Override
	public String toString() {
		return this.getDisplayId();
	}
}
