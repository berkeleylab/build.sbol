package org.sbolstandard.build.dom;

import java.util.List;
import java.util.UUID;

/**
 * An operator of the Build.SBOL language
 * 
 * @author Ernst Oberortner
 */
public class Operator 
		extends LanguageElement {

	// the operator's literal
	private BuildSBOLOperator operator;
	
	// the list of operands
	// since operators can be nested, the list of operands
	// can contain all types of language elements 
	// (i.e., operators and operands)
	private List<LanguageElement> operands;
	
	/**
	 * Constructor
	 * 
	 * @param operator ... the operator's literal
	 * @param operands ... the operands
	 */
	public Operator(final BuildSBOLOperator operator, final List<LanguageElement> operands) {
		super(operator + UUID.randomUUID().toString());	
			// at the moment, we use the operator's name and a random UUID as displayId
		
		this.operator = operator;
		this.operands = operands;
	}
	
	/**
	 * @return the operator's literal
	 */
	public BuildSBOLOperator getOperator() {
		return this.operator;
	}

	/**
	 * @return the list of operands
	 */
	public List<LanguageElement> getOperands() {
		return this.operands;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		sb.append(this.getOperator()).append("(");
		int i=0;
		for(LanguageElement operand : this.getOperands()) {
			sb.append(operand.toString());
			
			if(i<this.getOperands().size()-1) {
				sb.append(", ");
			}
			i++;
		}
		sb.append(")");
		return sb.toString();
	}
}
