package org.sbolstandard.build.dom;

/**
 * An operand of the Build.SBOL language
 * 
 * @author Ernst Oberortner
 */
public class Operand 
	extends LanguageElement {

	/**
	 * Constructor 
	 * 
	 * @param displayId ... the displayId of the operand 
	 */
	public Operand(final String displayId) {
		super(displayId);
	}
	
}
