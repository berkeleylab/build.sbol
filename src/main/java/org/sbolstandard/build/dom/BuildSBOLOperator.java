package org.sbolstandard.build.dom;

/**
 * allowed Build.SBOL operators
 * 
 * @author Ernst Oberortner
 *
 */
public enum BuildSBOLOperator {
	CUT, 
	JOIN,
	AMPLIFY,
	TRANSFORM	
}
