package org.sbolstandard.build.demo;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.sbolstandard.core2.AccessType;
import org.sbolstandard.core2.ComponentDefinition;
import org.sbolstandard.core2.SBOLConversionException;
import org.sbolstandard.core2.SBOLDocument;
import org.sbolstandard.core2.SBOLValidationException;
import org.sbolstandard.core2.SBOLWriter;
import org.sbolstandard.core2.Sequence;
import org.sbolstandard.core2.SequenceAnnotation;

/**
 * Given:
 * 2 linear DNA constructs (s1, s2)
 * 
 * Output:
 * 1 linear DNA construct (s), which 
 * is the assembly of s1 and s2
 * 
 * @author Ernst Oberortner
 *
 */
public class AssembleLinearConstructs {

	public static final String URI_PREFIX = "http://sbolstandard.org/assembly/example01/";

	/**
	 * 
	 * @param document
	 * @throws SBOLValidationException
	 */
	public static void defineComponents(final SBOLDocument document) 
			throws SBOLValidationException {
		
		//----------------------------------------------
		// linear DNA constructs
		//----------------------------------------------

		// s1
		// -- s1's ComponentDefinition
		ComponentDefinition s1 = document.createComponentDefinition(
				URI_PREFIX,
				"s1", 
				"1",
				ComponentDefinition.DNA);
		// -- s1's characteristics
		//    - DNA sequence
		Sequence s1Sequence = document.createSequence(
				URI_PREFIX,						// URI's prefix
				"s1_sequence",					// displayID
				"1",							// version
				"aaacccgggtttATGTTTTTATT", 		// elements
				org.sbolstandard.core2.Sequence.IUPAC_DNA);
												// encoding
		s1.addSequence(s1Sequence.getIdentity());

		//   - s1's sequence annotations
		//     - the assembly overlap
		SequenceAnnotation s1AssemblyOverlap = s1.createSequenceAnnotation("assembly_overlap", "assembly_overlap");
		s1AssemblyOverlap.addRange("assembly_overlap_range", 16, 23);
		//     - the vector overlap
		SequenceAnnotation s1VectorOverlap = s1.createSequenceAnnotation("vector_overlap", "vector_overlap");
		s1VectorOverlap.addRange("vector_overlap_range", 1, 12);

		//----------------------------------------------
		
		// s2
		// -- s2's ComponentDefinition
		ComponentDefinition s2 = document.createComponentDefinition(
				URI_PREFIX,
				"s2", 
				"1",
				ComponentDefinition.DNA);
		//----------------------------------------------
		// -- s2's characteristics
		//    - DNA sequence
		Sequence s2Sequence = document.createSequence(
				URI_PREFIX,						// URI's prefix
				"s2_sequence",					// displayID
				"1",							// version
				"TTTTTATTTGAtttgggcccaaa", 		// elements
				org.sbolstandard.core2.Sequence.IUPAC_DNA);
												// encoding
		s2.addSequence(s2Sequence.getIdentity());
		//   - s2's sequence annotations
		//     - the assembly overlap
		SequenceAnnotation s2AssemblyOverlap = s2.createSequenceAnnotation("assembly_overlap", "assembly_overlap");
		s2AssemblyOverlap.addRange("assembly_overlap_range", 1, 8);
		//     - the vector overlap
		SequenceAnnotation s2VectorOverlap = s2.createSequenceAnnotation("vector_overlap", "vector_overlap");
		s2VectorOverlap.addRange("vector_overlap_range", 12, 23);
		//----------------------------------------------
	}
	
	/**
	 * 
	 * @param document
	 * @throws SBOLValidationException
	 */
	public static void specifyBuildProcess(final SBOLDocument document) 
			throws SBOLValidationException {
		
		//--------------------------------------
		// PRIMERS
		//--------------------------------------
		// s1's 5' primer ("forward primer")
		ComponentDefinition s1ForwardPrimer = document.createComponentDefinition(
				URI_PREFIX,
				"s1_forward_primer", 
				"1",
				ComponentDefinition.DNA);
		Sequence s1ForwardPimerSequence = document.createSequence(
				URI_PREFIX,						// URI's prefix
				"s1_forward_primer_sequence",	// displayID
				"1",							// version
				"aaacccgggttt", 				// elements
				org.sbolstandard.core2.Sequence.IUPAC_DNA);
		s1ForwardPrimer.addSequence(s1ForwardPimerSequence);

		// s1's 3' primer ("reverse primer")
		ComponentDefinition s1ReversePrimer = document.createComponentDefinition(
				URI_PREFIX,
				"s1_reverse_primer", 
				"1",
				ComponentDefinition.DNA);
		Sequence s1ReversePimerSequence = document.createSequence(
				URI_PREFIX,						// URI's prefix
				"s1_reverse_primer_sequence",	// displayID
				"1",							// version
				"aataaaa", 						// elements
				org.sbolstandard.core2.Sequence.IUPAC_DNA);
		s1ReversePrimer.addSequence(s1ReversePimerSequence);

		// s2's 5' primer ("forward primer")
		ComponentDefinition s2ForwardPrimer = document.createComponentDefinition(
				URI_PREFIX,
				"s2_forward_primer", 
				"1",
				ComponentDefinition.DNA);
		Sequence s2ForwardPimerSequence = document.createSequence(
				URI_PREFIX,						// URI's prefix
				"s2_forward_primer_sequence",	// displayID
				"1",							// version
				"TTTTTATT", 					// elements
				org.sbolstandard.core2.Sequence.IUPAC_DNA);
		s2ForwardPrimer.addSequence(s2ForwardPimerSequence);

		// s2's 3' primer ("reverse primer")
		ComponentDefinition s2ReversePrimer = document.createComponentDefinition(
				URI_PREFIX,
				"s2_reverse_primer", 
				"1",
				ComponentDefinition.DNA);
		Sequence s2ReversePimerSequence = document.createSequence(
				URI_PREFIX,						// URI's prefix
				"s2_reverse_primer_sequence",	// displayID
				"1",							// version
				"tttggg", 						// elements
				org.sbolstandard.core2.Sequence.IUPAC_DNA);
		s2ReversePrimer.addSequence(s2ReversePimerSequence);
		//--------------------------------------

		
		ComponentDefinition amplifiedS1 = document.createComponentDefinition(
				URI_PREFIX,
				"amplified_s1", 
				"1",
				ComponentDefinition.DNA);
		amplifiedS1.createComponent("s1", AccessType.PUBLIC, document.getComponentDefinition("s1", "1").getIdentity());
		amplifiedS1.createComponent("s1_forward_primer", AccessType.PUBLIC, document.getComponentDefinition("s1_forward_primer", "1").getIdentity());
		amplifiedS1.createComponent("s1_reverse_primer", AccessType.PUBLIC, document.getComponentDefinition("s1_reverse_primer", "1").getIdentity());

		ComponentDefinition amplifiedS2 = document.createComponentDefinition(
				URI_PREFIX,
				"amplified_s2", 
				"1",
				ComponentDefinition.DNA);
		amplifiedS2.createComponent("s2", AccessType.PUBLIC, document.getComponentDefinition("s2", "1").getIdentity());
		amplifiedS2.createComponent("s2_forward_primer", AccessType.PUBLIC, document.getComponentDefinition("s2_forward_primer", "1").getIdentity());
		amplifiedS2.createComponent("s2_reverse_primer", AccessType.PUBLIC, document.getComponentDefinition("s2_reverse_primer", "1").getIdentity());

	}
	
	/**
	 * 
	 * @param args
	 * 
	 * @throws SBOLValidationException
	 * @throws IOException
	 * @throws SBOLConversionException
	 */
	public static void main(String[] args) 
			throws SBOLValidationException, IOException, SBOLConversionException {
		
		// instantiate a SBOL document
		SBOLDocument document = new SBOLDocument();
		document.setDefaultURIprefix(URI_PREFIX); 
		
		// specify the components of the assembly process
		// i.e., linear DNA and circular vector
		defineComponents(document);
		
		// specify the assembly process itself
		// step-by-step
		specifyBuildProcess(document);
		
		// serialize document
		Path outputFile = Paths.get(".", "data", "assemble_linear_constructs.sbol");
		SBOLWriter.write(document, outputFile.toFile());
	}

}