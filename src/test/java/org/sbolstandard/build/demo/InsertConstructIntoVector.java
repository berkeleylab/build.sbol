package org.sbolstandard.build.demo;

import java.io.IOException;
import java.net.URI;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Set;

import org.sbolstandard.build.BuildSBOLOperators;
import org.sbolstandard.core2.ComponentDefinition;
import org.sbolstandard.core2.SBOLConversionException;
import org.sbolstandard.core2.SBOLDocument;
import org.sbolstandard.core2.SBOLValidationException;
import org.sbolstandard.core2.SBOLWriter;
import org.sbolstandard.core2.Sequence;

/**
 * given components:
 * 1 linear DNA construct, 1 vector
 * 
 * output: SBOL document containing
 * -- given components and
 * -- the process of inserting construct into vector
 * 
 * @author Ernst Oberortner
 */
public class InsertConstructIntoVector {

	public static final String URI_PREFIX = "http://sbolstandard.org/build/example01";

	public static final URI ENZYME = URI.create("http://www.biopax.org/release/biopax-level3.owl#ModificationFeature");
	/*
	 * we represent any enzyme that opens a vector as BioPAX ModificationFeature
	 * 
	 * BioPAX Definition: ModificationFeature
	 * Cleavage of a circular sequence e.g. a plasmid. In the case of removal 
	 * (e.g. intron) the fragment that is *removed* is specified in the feature location property. 
	 * In the case of a "cut" (e.g. restriction enzyme cut site) the location of the cut is specified instead. 
	 */
	
	// we represent 5' and 3' primers for construct amplification using the following SequenceOntology (SO) terms
	// -- forward primer: A single stranded oligo used for polymerase chain reaction
	public static final URI FORWARD_PRIMER = URI.create("http://www.sequenceontology.org/browser/release_2.1/term/SO:0000121");
	// -- reverse primer: A single stranded oligo used for polymerase chain reaction
	public static final URI REVERSE_PRIMER = URI.create("http://www.sequenceontology.org/browser/release_2.1/term/SO:0000132");
	
	
	/**
	 * 
	 * @param document
	 * @throws SBOLValidationException
	 */
	public static void defineComponents(final SBOLDocument document) 
			throws SBOLValidationException {

		//----------------------------------------------
		// linear DNA construct
		//----------------------------------------------
		// -- the construct's ComponentDefinition
		ComponentDefinition dnaConstruct = document.createComponentDefinition(
				URI_PREFIX,
				"linear_construct", 
				"1",
				ComponentDefinition.DNA);
		//----------------------------------------------
		// -- the construct's characteristics
		//    - DNA sequence
		Sequence dnaConstructSequence = document.createSequence(
				URI_PREFIX,						// URI's prefix
				"construct_sequence",		// displayID
				"1",							// version
				"ATGTTTTTATTTTGA", 				// elements
				org.sbolstandard.core2.Sequence.IUPAC_DNA);
												// encoding
		dnaConstruct.addSequence(dnaConstructSequence.getIdentity());
		//----------------------------------------------

		//----------------------------------------------
		// Vector
		//----------------------------------------------
		ComponentDefinition vector = document.createComponentDefinition(
				URI_PREFIX,
				"vector", 
				"1",
				ComponentDefinition.DNA);
		//----------------------------------------------
		// -- the vector's characteristics
		//    - the vector's sequence (OPTIONAL)
		Sequence vectorSequence = document.createSequence(
				URI_PREFIX,						// URI's prefix
				"vector_sequence",				// displayID
				"1",							// version
				"AAAAAAAAAAAAAAAAAA",			// elements
				org.sbolstandard.core2.Sequence.IUPAC_DNA);
												// encoding
		vector.addSequence(vectorSequence.getIdentity());
	}
	
	/**
	 * 
	 * @param document
	 * @throws SBOLValidationException
	 */
	public static void specifyBuildProcess(final SBOLDocument document) 
			throws SBOLValidationException {
	
		// add additional components that are required in the assembly process
		// i.e., enzymes (to cut the vector open), oligos (to amplify the DNA constructs)
		addBuildSpecificComponents(document);

		//---------------------------------------------------------------------
		// BUILD.SBOL SPECIFICATIONS
		//---------------------------------------------------------------------
		// Operation 1: we linearize the vector using a restriction enzyme
		ComponentDefinition circularVector = document.getComponentDefinition("vector", "1");
		ComponentDefinition restrictionEnzyme = document.getComponentDefinition("restriction_enzyme", "1");
		Set<ComponentDefinition> linearizedVector = BuildSBOLOperators.cut(
						document, "linearized_vector", circularVector, restrictionEnzyme);

		// Operation 2: we amplify the linear DNA construct with its 5' and 3' primers
		ComponentDefinition linearConstruct = document.getComponentDefinition("linear_construct", "1");
		ComponentDefinition forwardPrimer = document.getComponentDefinition("construct_forward_primer", "1");
		ComponentDefinition reversePrimer = document.getComponentDefinition("construct_reverse_primer", "1");
		ComponentDefinition amplifiedConstruct = BuildSBOLOperators.amplify(
				document, "amplified_construct", linearConstruct, forwardPrimer, reversePrimer);

		// Operation 3: we explicitly specify the join of the linearized_vector and the amplified_construct
		BuildSBOLOperators.join(
				document, "assembled_product", (new ArrayList<ComponentDefinition>(linearizedVector)).get(0), amplifiedConstruct);
		//---------------------------------------------------------------------
	}
	
	/**
	 * 
	 * @param document
	 * @throws SBOLValidationException
	 */
	public static void addBuildSpecificComponents(final SBOLDocument document) 
			throws SBOLValidationException {
		
		//----------------------------------------------
		// ENZYMES
		//----------------------------------------------
		// -- smaI
		document.createComponentDefinition(
				URI_PREFIX,
				"restriction_enzyme", 
				"1",
				ENZYME);

		//----------------------------------------------
		// OLIGOS / PRIMERS
		//----------------------------------------------
		// -- the construct's 5' primer
		ComponentDefinition constructForwardPrimer = document.createComponentDefinition(
				URI_PREFIX,
				"construct_forward_primer", 
				"1",
				FORWARD_PRIMER);
		//    and its characteristics
		//    - sequence
		Sequence constructForwardPrimerSequence = document.createSequence(
				URI_PREFIX,									// URI's prefix
				"construct_forward_primer_sequence",		// displayID
				"1",										// version
				"ATGTT",									// elements
				org.sbolstandard.core2.Sequence.IUPAC_DNA);	// encoding
		constructForwardPrimer.addSequence(constructForwardPrimerSequence.getIdentity());
		
		// -- the construct's 3' primer
		ComponentDefinition constructReversePrimer = document.createComponentDefinition(
				URI_PREFIX,
				"construct_reverse_primer", 
				"1",
				REVERSE_PRIMER);
		//    and its characteristics
		//    - sequence
		Sequence constructReversePrimerSequence = document.createSequence(
				URI_PREFIX,									// URI's prefix
				"construct_reverse_primer_sequence",		// displayID
				"1",										// version
				"TCAAA",									// elements
				org.sbolstandard.core2.Sequence.IUPAC_DNA);	// encoding
		constructReversePrimer.addSequence(constructReversePrimerSequence.getIdentity());
		//----------------------------------------------

	}
	
	/**
	 * 
	 * @param args ... no args supported 
	 * 
	 * @throws SBOLValidationException
	 * @throws SBOLConversionException
	 * @throws IOException
	 */
	public static void main(String[] args) 
			throws SBOLValidationException, SBOLConversionException, IOException {

		// instantiate a SBOL document
		SBOLDocument document = new SBOLDocument();
		document.setDefaultURIprefix(URI_PREFIX); 
		
		// specify the components of the build process
		// i.e., linear DNA and circular vector
		defineComponents(document);
		
		// specify the assembly process itself
		// step-by-step
		specifyBuildProcess(document);
		
		// serialize document
		Path outputFile = Paths.get(".", "data", "insert_construct_into_vector.sbol");
		SBOLWriter.write(document, outputFile.toFile());
	}
	
	
}