package org.sbolstandard.build.demo;

import java.io.ByteArrayInputStream;
import java.nio.file.Paths;

import org.sbolstandard.build.BuildSBOLAdapter;
import org.sbolstandard.build.BuildSBOLConstants;
import org.sbolstandard.build.BuildSBOLOperators;
import org.sbolstandard.build.InterpResults;
import org.sbolstandard.build.ParseResults;
import org.sbolstandard.core2.ComponentDefinition;
import org.sbolstandard.core2.SBOLDocument;
import org.sbolstandard.core2.SBOLWriter;

/**
* Here, we exemplify how to specify a build operations using SBOL's PROV-O support.
* 
* The build operations are:
* -- cut     ... linearize a (circular) vector using a restriction enzyme.
* -- amplify ... amplification of a linear or circular construct using 5' and 3' primers
* -- join    ... assembly of two construct using a ligase  
* 
* 
* NOTE! All operations are protocol-agnostic. That is, we do not specify HOW
*       these operations should be executed in the lab.
*       
* @author Ernst Oberortner
* 
*/
public class SpecifyBuildOperations {
	
	// TODO:
	// add "Build" ontology term to (at least) operations

	/**
	 * MAIN
	 * 
	 * @param args
	 * 
	 * @throws Exception
	 */
	public static void main(String[] args) 
			throws Exception {

		// exemplify the cut operation
		specifyCutOperation();
		
		// exemplify the amplify operation 
		specifyAmplifyOperation();
		
		// exemplify the join operation
		specifyJoinOperation();
		
		// exemplify a "composite" operation
		specifyCompositeOperation();
	}

	/**
	 * specifies to perform a cut operation in order to linearize a vector using a restriction enzyme
	 * 
	 * @throws Exception
	 */
	public static void specifyCutOperation() 
			throws Exception {

		// instantiate a document
		SBOLDocument document = new SBOLDocument();				
		document.setDefaultURIprefix(BuildSBOLConstants.BUILD_PREFIX);
		
		ComponentDefinition vector = document.createComponentDefinition(
				"vector", BuildSBOLConstants.VECTOR_PLASMID);
		vector.setName("vector");
		
		ComponentDefinition enzyme = document.createComponentDefinition(
				"restriction_enzyme", BuildSBOLConstants.RESTRICTION_ENZYME);
		enzyme.setName("restriction_enzyme");

		// specify the cut operation
		BuildSBOLOperators.cut(
				document, "linearized_vector", vector, enzyme);
		
		// serialize the document to a file
		SBOLWriter.write(document, Paths.get("./data/cut_operation.sbol.xml").toFile());
   }
	
	
	/**
	 * specifies the amplify operation, which amplifies a linear DNA construct using 
	 * 5' and 3' primers
	 * 
	 * @throws Exception
	 */
	public static void specifyAmplifyOperation() 
			throws Exception {
		
		// instantiate a document
		SBOLDocument document = new SBOLDocument();				
		document.setDefaultURIprefix(BuildSBOLConstants.BUILD_PREFIX);
		
		// the linear DNA construct
		ComponentDefinition dnaConstruct = document.createComponentDefinition(
				"dna_construct", BuildSBOLConstants.LINEAR_SINGLE_STRANDED_DNA);
		dnaConstruct.setName("dna_construct");
		
		// the 5' primer for amplification
		ComponentDefinition fivePrimer = document.createComponentDefinition(
				"five_primer", BuildSBOLConstants.FORWARD_PRIMER);
		fivePrimer.setName("five_primer");
		
		// the 3' primer for amplification
		ComponentDefinition threePrimer = document.createComponentDefinition(
				"three_primer", BuildSBOLConstants.REVERSE_PRIMER);
		threePrimer.setName("three_primer");

		// specify the amplification operation
		BuildSBOLOperators.amplify(
				document, "amplified_construct", dnaConstruct, fivePrimer, threePrimer);
				
		// serialize the document to a file
		SBOLWriter.write(document, Paths.get("./data/amplify_operation.sbol.xml").toFile());
	}


	/**
	 * specifies a join operation, which joins two linear DNA constructs
	 * 
	 * NOTE! at this point, we do not specify any further information 
	 * about how to execute the join operation!
	 * 
	 * @throws Exception
	 */
	public static void specifyJoinOperation() 
			throws Exception {
		
		// instantiate a document
		SBOLDocument document = new SBOLDocument();				
		document.setDefaultURIprefix(BuildSBOLConstants.BUILD_PREFIX);

		// the first linear DNA construct
		ComponentDefinition cdPart1 = document.createComponentDefinition(
				"dna_part_1", BuildSBOLConstants.LINEAR_DOUBLE_STRANDED_DNA);
		cdPart1.setName("dna_part_1");

		// the second linear DNA construct
		ComponentDefinition cdPart2 = document.createComponentDefinition(
				"dna_part_2", BuildSBOLConstants.LINEAR_DOUBLE_STRANDED_DNA);
		cdPart2.setName("dna_part_2");

		BuildSBOLOperators.join(
				document, "joined_dna_construct", cdPart1, cdPart2);
		
		// serialize the document to a file
		SBOLWriter.write(document, Paths.get("./data/join_operation.sbol.xml").toFile());
	}
	
	public static void specifyCompositeOperation() 
			throws Exception {
		
		String buildSbolScript = "join ( \n" + 
				"	amplify ( construct, polymerase ),\n" + 
				"	cut ( vector, restriction_enzyme )  )\n" + 
				"";

		// first, parse the specification
		ParseResults nestedSpecResults = BuildSBOLAdapter.parse(new ByteArrayInputStream(buildSbolScript.getBytes()));

		// then, interpret the results of the parsing step
		// herefore, we need an SBOL document that contains  
		// a ComponentDefinition for each operand
		SBOLDocument nestedSpecDocument = new SBOLDocument();
		nestedSpecDocument.setDefaultURIprefix("https://sbolstandard.org/build");

		nestedSpecDocument.createComponentDefinition("construct", "1", BuildSBOLConstants.LINEAR_SINGLE_STRANDED_DNA);
		nestedSpecDocument.createComponentDefinition("polymerase", "1", BuildSBOLConstants.POLYMERASE);
		nestedSpecDocument.createComponentDefinition("restriction_enzyme", "1", BuildSBOLConstants.RESTRICTION_ENZYME);
		nestedSpecDocument.createComponentDefinition("vector", "1", BuildSBOLConstants.VECTOR_PLASMID);

		InterpResults results = BuildSBOLAdapter.interpret(nestedSpecDocument, nestedSpecResults.buildSpecifications);
		// serialize the document to a file
		SBOLWriter.write(nestedSpecDocument, Paths.get("./data/composite_operation.sbol.xml").toFile());

	}
}