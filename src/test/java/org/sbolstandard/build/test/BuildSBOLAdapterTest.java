package org.sbolstandard.build.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayInputStream;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;
import org.sbolstandard.build.BuildSBOLAdapter;
import org.sbolstandard.build.BuildSBOLConstants;
import org.sbolstandard.build.InterpResults;
import org.sbolstandard.build.ParseResults;
import org.sbolstandard.build.dom.Operator;
import org.sbolstandard.core2.ComponentDefinition;
import org.sbolstandard.core2.SBOLDocument;

public class BuildSBOLAdapterTest {

	private static final String URI_PREFIX = "https://sbolstandard.org/build/test";

	@Test
	public void testAssignments() {
		
		try { 
			// amplify construct c1 w/ enzyme e1
			String spec = "X = amplify ( c1, e1 )";
			
			ParseResults parserResults = BuildSBOLAdapter.parse(new ByteArrayInputStream(spec.getBytes()));
			assertNotNull(parserResults.buildSpecifications);
			assertNull(parserResults.exceptions);
			
			// interpret the results
			// but first create a SBOL document containing the componentdefinitions
			// of the primitives (i.e., c1 and e1)
			SBOLDocument document = new SBOLDocument();
			document.setDefaultURIprefix(URI_PREFIX);

			document.createComponentDefinition("c1", "1", BuildSBOLConstants.LINEAR_SINGLE_STRANDED_DNA);
			document.createComponentDefinition("e1", "1", BuildSBOLConstants.RESTRICTION_ENZYME);

			InterpResults interpResults = BuildSBOLAdapter.interpret(document, parserResults.buildSpecifications);
			assertNull(interpResults.getExceptions());

			assertEquals(interpResults.getOperatorsAndComponentDefinitions().size(), 1);
			
			Operator op = parserResults.buildSpecifications.get("X");
			assertNotNull(op);
			assertNotNull(interpResults.getOperatorsAndComponentDefinitions().get(op));

		} catch(Exception e) {
			assertTrue(false);
		}

		try {
			// specify the amplification twice
			// should result in only specifying it once in build.sbol
			String spec = 
					"X = amplify ( c1, e1 )" +
					"X = amplify ( c1, e1 )"; 

			ParseResults parserResults = BuildSBOLAdapter.parse(new ByteArrayInputStream(spec.getBytes()));
			assertNotNull(parserResults.buildSpecifications);
			assertNull(parserResults.exceptions);
			
			// interpret the results
			// but first create a SBOL document containing the componentdefinitions
			// of the primitives (i.e., c1 and e1)
			SBOLDocument document = new SBOLDocument();
			document.setDefaultURIprefix(URI_PREFIX);

			document.createComponentDefinition("c1", "1", BuildSBOLConstants.LINEAR_SINGLE_STRANDED_DNA);
			document.createComponentDefinition("e1", "1", BuildSBOLConstants.RESTRICTION_ENZYME);

			InterpResults interpResults = BuildSBOLAdapter.interpret(document, parserResults.buildSpecifications);
			assertNull(interpResults.getExceptions());

			assertEquals(interpResults.getOperatorsAndComponentDefinitions().size(), 1);
			
			Operator op = parserResults.buildSpecifications.get("X");
			assertNotNull(op);
			assertNotNull(interpResults.getOperatorsAndComponentDefinitions().get(op));

		} catch(Exception e) {
			assertTrue(false);
		}

	}

	@Test
	public void testAmplify() {
		
		// amplify construct c1 w/ enzyme e1
		String spec = "amplify ( c1, e1 )";
		
		ParseResults results = BuildSBOLAdapter.parse(new ByteArrayInputStream(spec.getBytes()));
		assertNotNull(results.buildSpecifications);
		assertNull(results.exceptions);

	}
	
	@Test
	public void testParseAndInterpret() {

		//------------------------------------
		// UNDEFINED OPERANDS
		//------------------------------------
		
		// NESTED OPERATION
		// amplify construct c1 w/ enzyme e1
		String nestedSpec = "join ( amplify ( c1, e1 ), cut ( v1, e1 )  )";
		
		// first, parse the specification
		ParseResults nestedSpecResults = BuildSBOLAdapter.parse(new ByteArrayInputStream(nestedSpec.getBytes()));
		assertNotNull(nestedSpecResults.buildSpecifications);
		assertNull(nestedSpecResults.exceptions);

		// then, interpret the results of the parsing step
		// herefore, we need an SBOL document that contains  
		// a ComponentDefinition for each operand
		SBOLDocument nestedSpecDocument = new SBOLDocument();
		nestedSpecDocument.setDefaultURIprefix(URI_PREFIX);

		try {
			InterpResults results = BuildSBOLAdapter.interpret(nestedSpecDocument, nestedSpecResults.buildSpecifications);
			assertNotNull(results.getExceptions());
			assertNull(results.getOperatorsAndComponentDefinitions());
		} catch(Exception e) {
			assertTrue(false);
		}
		
		
		// MULTIPLE OPERATIONS
		// amplify construct c1 w/ enzyme e1
		String multiNestedSpec = 
				"join ( amplify ( c1, e1 ), cut ( v1, e1 )  )" + 
				"join ( amplify ( c2, e2 ), cut ( v2, e2 )  )"; 
		
		// first, parse the specification
		ParseResults multiNestedSpecResults = BuildSBOLAdapter.parse(
				new ByteArrayInputStream(multiNestedSpec.getBytes()));
		assertNotNull(multiNestedSpecResults.buildSpecifications);
		assertNull(multiNestedSpecResults.exceptions);

		// then, interpret the results of the parsing step
		// herefore, we need an SBOL document that contains  
		// a ComponentDefinition for each operand
		SBOLDocument multiNestedSpecDocument = new SBOLDocument();
		multiNestedSpecDocument.setDefaultURIprefix(URI_PREFIX);
		
		try {
			InterpResults results = BuildSBOLAdapter.interpret(multiNestedSpecDocument, multiNestedSpecResults.buildSpecifications);
			assertNotNull(results.getExceptions());
			assertNull(results.getOperatorsAndComponentDefinitions());
		} catch(Exception e) {
			assertTrue(false);
		}

		//------------------------------------
		// DEFINED OPERANDS
		//------------------------------------

		// NESTED OPERATION
		// amplify construct c1 w/ enzyme e1
		nestedSpec = "join ( amplify ( c1, e1 ), cut ( v1, e1 )  )";
		
		// first, parse the specification
		nestedSpecResults = BuildSBOLAdapter.parse(new ByteArrayInputStream(nestedSpec.getBytes()));
		assertNotNull(nestedSpecResults.buildSpecifications);
		assertNull(nestedSpecResults.exceptions);

		// then, interpret the results of the parsing step
		// herefore, we need an SBOL document that contains  
		// a ComponentDefinition for each operand
		nestedSpecDocument = new SBOLDocument();
		nestedSpecDocument.setDefaultURIprefix(URI_PREFIX);
		try {
			nestedSpecDocument.createComponentDefinition("c1", "1", BuildSBOLConstants.LINEAR_SINGLE_STRANDED_DNA);
			nestedSpecDocument.createComponentDefinition("e1", "1", BuildSBOLConstants.RESTRICTION_ENZYME);
			nestedSpecDocument.createComponentDefinition("v1", "1", BuildSBOLConstants.VECTOR_PLASMID);
		} catch(Exception e) {
			assertTrue(false);
		}

		try {
			InterpResults results = BuildSBOLAdapter.interpret(nestedSpecDocument, nestedSpecResults.buildSpecifications);
			
			
			assertNull(results.getExceptions());
			assertNotNull(results.getOperatorsAndComponentDefinitions());
			// there should have been 1 ComponentDefinitions (for the join operation)
			assertEquals(results.getOperatorsAndComponentDefinitions().size(), 1);
		} catch(Exception e) {
			assertTrue(false);
		}
		
		// MULTIPLE OPERATIONS
		// amplify construct c1 w/ enzyme e1
		multiNestedSpec = 
				"join ( amplify ( c1, e1 ), cut ( v1, e1 )  )" + 
				"join ( amplify ( c2, e2 ), cut ( v2, e2 )  )"; 
		
		// first, parse the specification
		multiNestedSpecResults = BuildSBOLAdapter.parse(
				new ByteArrayInputStream(multiNestedSpec.getBytes()));
		assertNotNull(multiNestedSpecResults.buildSpecifications);
		assertNull(multiNestedSpecResults.exceptions);

		// then, interpret the results of the parsing step
		// herefore, we need an SBOL document that contains  
		// a ComponentDefinition for each operand
		multiNestedSpecDocument = new SBOLDocument();
		multiNestedSpecDocument.setDefaultURIprefix(URI_PREFIX);
		try {
			multiNestedSpecDocument.createComponentDefinition("c1", "1", BuildSBOLConstants.LINEAR_SINGLE_STRANDED_DNA);
			multiNestedSpecDocument.createComponentDefinition("c2", "1", BuildSBOLConstants.LINEAR_SINGLE_STRANDED_DNA);
			multiNestedSpecDocument.createComponentDefinition("e1", "1", BuildSBOLConstants.RESTRICTION_ENZYME);
			multiNestedSpecDocument.createComponentDefinition("e2", "1", BuildSBOLConstants.RESTRICTION_ENZYME);
			multiNestedSpecDocument.createComponentDefinition("v1", "1", BuildSBOLConstants.VECTOR_PLASMID);
			multiNestedSpecDocument.createComponentDefinition("v2", "1", BuildSBOLConstants.VECTOR_PLASMID);
		} catch(Exception e) {
			assertTrue(false);
		}
		
		try {
			InterpResults results = BuildSBOLAdapter.interpret(multiNestedSpecDocument, multiNestedSpecResults.buildSpecifications);
			assertNull(results.getExceptions());
			assertNotNull(results.getOperatorsAndComponentDefinitions());
			// there should have been 2 ComponentDefinitions (for the two join operations)
			assertEquals(results.getOperatorsAndComponentDefinitions().size(), 2);
		} catch(Exception e) {
			assertTrue(false);
		}
	}
	
	@Test
	public void testDuplicateCutOperation() {
		String duplicateCutSpec = 
				"cut ( v1, e1 )" + 
				"cut ( v1, e1 )"; 
		SBOLDocument document = new SBOLDocument();
		document.setDefaultURIprefix(URI_PREFIX);
		try {
			document.createComponentDefinition("e1", "1", BuildSBOLConstants.RESTRICTION_ENZYME);
			document.createComponentDefinition("v1", "1", BuildSBOLConstants.VECTOR_PLASMID);
		} catch(Exception e) {
			assertTrue(false);
		}

		// first, parse the specification
		ParseResults duplicateCutSpecResults = BuildSBOLAdapter.parse(
				new ByteArrayInputStream(duplicateCutSpec.getBytes()));
		assertNotNull(duplicateCutSpecResults.buildSpecifications);
		assertNull(duplicateCutSpecResults.exceptions);

		try {
			InterpResults results = BuildSBOLAdapter.interpret(document, duplicateCutSpecResults.buildSpecifications);
			assertNull(results.getExceptions());
			assertNotNull(results.getOperatorsAndComponentDefinitions());
			// there should have been 2 ComponentDefinitions (for the two join operations)
			assertEquals(results.getOperatorsAndComponentDefinitions().size(), 2);
			
			// accumulate the CDs of the operators
			Set<ComponentDefinition> cds = new HashSet<>();
			Set<ComponentDefinition> cd1 = null;
			Set<ComponentDefinition> cd2 = null;
			for(Operator op : results.getOperatorsAndComponentDefinitions().keySet()) {
				if(cd1 == null) {
					cd1 = results.getOperatorsAndComponentDefinitions().get(op);
					assertEquals(cd1.size(), 1);
				} else if(cd2 == null) {
					cd2 = results.getOperatorsAndComponentDefinitions().get(op); 
					assertEquals(cd2.size(), 1);
				}
				cds.addAll(results.getOperatorsAndComponentDefinitions().get(op));
			}
			// here, there should be 2 CDs
			assertEquals(cds.size(), 2);
			assertNotNull(cd1);
			assertNotNull(cd2);
			assertNotEquals(cd1, cd2);
			
			// there should be 1 Activity
			assertEquals(document.getActivities().size(), 1);
		} catch(Exception e) {
			assertTrue(false);
		}

	}
}
